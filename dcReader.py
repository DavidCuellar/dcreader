import sys
import maya.api.OpenMaya as om
import math


def maya_useNewAPI():
    """
    The presence of this function tells Maya that the plugin produces, and
    expects to be passed, objects created using the Maya Python API 2.0.
    """
    pass


class dcReader(om.MPxNode):
    id = om.MTypeId(0x80005)
    reader = om.MObject()
    target = om.MObject()
    angleX = om.MObject()
    angleY = om.MObject()
    angleZ = om.MObject()
    xValues = om.MObject()
    yValues = om.MObject()
    zValues = om.MObject()
    message = om.MObject()
    xPosRamp = om.MRampAttribute()
    xNegRamp = om.MRampAttribute()
    yPosRamp = om.MRampAttribute()
    yNegRamp = om.MRampAttribute()
    zPosRamp = om.MRampAttribute()
    zNegRamp = om.MRampAttribute()

    @staticmethod
    def creator():
        return dcReader()

    @staticmethod
    def initialize():
        ##################
        # INPUT ATTRIBUTES
        ##################
        mtxAttr = om.MFnMatrixAttribute()
        # Reader attribute
        dcReader.reader = mtxAttr.create('Reader_Matrix', 'RM', om.MFnMatrixData.kMatrix)
        om.MPxNode.addAttribute(dcReader.reader)
        mtxAttr.writable = True
        mtxAttr.hidden = False
        mtxAttr.keyable = True
        mtxAttr.disconnectBehavior = 2

        # Target attribute
        dcReader.target = mtxAttr.create('Target_Matrix', 'TM', om.MFnMatrixData.kMatrix)
        om.MPxNode.addAttribute(dcReader.target)
        mtxAttr.writable = True
        mtxAttr.hidden = False
        mtxAttr.keyable = True
        mtxAttr.disconnectBehavior = 2

        ###################
        # OUTPUT ATTRIBUTES
        ###################
        nFn = om.MFnNumericAttribute()
        # X angle
        dcReader.angleX = nFn.create('angleX', 'ax', om.MFnNumericData.kFloat)
        om.MPxNode.addAttribute(dcReader.angleX)
        nFn.writable = False
        nFn.keyable = False

        # Y angle
        dcReader.angleY = nFn.create('angleY', 'ay', om.MFnNumericData.kFloat)
        om.MPxNode.addAttribute(dcReader.angleY)
        nFn.writable = False
        nFn.keyable = False

        # Z angle
        dcReader.angleZ = nFn.create('angleZ', 'az', om.MFnNumericData.kFloat)
        om.MPxNode.addAttribute(dcReader.angleZ)
        nFn.writable = False
        nFn.keyable = False

        # X axis output
        dcReader.xValues = nFn.create('X_output', 'XO', om.MFnNumericData.k2Float)
        om.MPxNode.addAttribute(dcReader.xValues)
        nFn.writable = False
        nFn.keyable = False

        # Y axis output
        dcReader.yValues = nFn.create('Y_output', 'YO', om.MFnNumericData.k2Float)
        om.MPxNode.addAttribute(dcReader.yValues)
        nFn.writable = False
        nFn.keyable = False

        # Z axis output
        dcReader.zValues = nFn.create('Z_output', 'ZO', om.MFnNumericData.k2Float)
        om.MPxNode.addAttribute(dcReader.zValues)
        nFn.writable = False
        nFn.keyable = False

        # Info attribute
        stringFn = om.MFnStringData().create('dcReader. v1.0. David Cuellar. www.davidcuellar.es')
        stringAttr = om.MFnTypedAttribute()
        dcReader.message = stringAttr.create("Info", "inf", om.MFnData.kString, stringFn)
        dcReader.addAttribute(dcReader.message)
        stringAttr.storable = True
        stringAttr.readable = True
        stringAttr.writable = False
        stringAttr.keyable = False

        #####################
        # INTERNAL ATTRIBUTES
        #####################
        # Ramp Curve attributes
        ramp = om.MRampAttribute()
        dcReader.xPosRamp = ramp.createCurveRamp('X_positive', 'xp')
        om.MPxNode.addAttribute(dcReader.xPosRamp)
        om.MPxNode.attributeAffects(dcReader.xPosRamp, dcReader.xValues)

        dcReader.xNegRamp = ramp.createCurveRamp('X_negative', 'xn')
        om.MPxNode.addAttribute(dcReader.xNegRamp)
        om.MPxNode.attributeAffects(dcReader.xNegRamp, dcReader.xValues)

        dcReader.yPosRamp = ramp.createCurveRamp('Y_positive', 'yp')
        om.MPxNode.addAttribute(dcReader.yPosRamp)
        om.MPxNode.attributeAffects(dcReader.yPosRamp, dcReader.yValues)

        dcReader.yNegRamp = ramp.createCurveRamp('Y_negative', 'yn')
        om.MPxNode.addAttribute(dcReader.yNegRamp)
        om.MPxNode.attributeAffects(dcReader.yNegRamp, dcReader.yValues)

        dcReader.zPosRamp = ramp.createCurveRamp('Z_positive', 'zp')
        om.MPxNode.addAttribute(dcReader.zPosRamp)
        om.MPxNode.attributeAffects(dcReader.zPosRamp, dcReader.zValues)

        dcReader.zNegRamp = ramp.createCurveRamp('Z_negative', 'zn')
        om.MPxNode.addAttribute(dcReader.zNegRamp)
        om.MPxNode.attributeAffects(dcReader.zNegRamp, dcReader.zValues)

        ###################
        # Attribute affects
        ###################
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.xValues)
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.yValues)
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.zValues)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.xValues)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.yValues)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.zValues)
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.angleX)
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.angleY)
        om.MPxNode.attributeAffects(dcReader.reader, dcReader.angleZ)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.angleX)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.angleY)
        om.MPxNode.attributeAffects(dcReader.target, dcReader.angleZ)

    def __init__(self):
        om.MPxNode.__init__(self)

    def compute(self, plug, data):
        # Get the plugs of this node
        thisNode = self.thisMObject()
        readerPlug = om.MPlug(thisNode, dcReader.reader)
        targetPlug = om.MPlug(thisNode, dcReader.target)

        # Get the connect plugs of the node
        plugConnections = om.MFnDependencyNode(thisNode).getConnections()

        # Check if the plugs are connected
        if readerPlug in plugConnections and targetPlug in plugConnections:

            # Read the input values
            readerMtx = data.inputValue(dcReader.reader).asFloatMatrix()
            targetMtx = data.inputValue(dcReader.target).asFloatMatrix()

            # return the vectors from the input matrix
            rx, ry, rz, rt = self.return_matrix_vectors(readerMtx)
            tx, ty, tz, tt = self.return_matrix_vectors(targetMtx)

            # Calculate the aim vector from the reader to the target
            aimV = self.get_aim_vector(rt, tt)

            # Get the angles between each reader axis and the Aim target vector
            ax, ay, az = self.get_angles(rx, ry, rz, aimV)

            # Get the rotation values normalize
            xp, xn, yp, yn, zp, zn = self.rotation_values_normalize(ax, ay, az)

            # Read the ramp value for each rotation value
            xp, xn, yp, yn, zp, zn = self.get_ramp_values(xp, xn, yp, yn, zp, zn)

            # OUTPUTS
            angleXOut = data.outputValue(dcReader.angleX)
            angleXOut.setFloat(ax)
            data.setClean(dcReader.angleX)

            angleYOut = data.outputValue(dcReader.angleY)
            angleYOut.setFloat(ay)
            data.setClean(dcReader.angleY)

            angleZOut = data.outputValue(dcReader.angleZ)
            angleZOut.setFloat(az)
            data.setClean(dcReader.angleZ)

            xOutput = data.outputValue(dcReader.xValues)
            xOutput.set2Float(xp, xn)
            data.setClean(dcReader.xValues)

            yOutput = data.outputValue(dcReader.yValues)
            yOutput.set2Float(yp, yn)
            data.setClean(dcReader.yValues)

            zOutput = data.outputValue(dcReader.zValues)
            zOutput.set2Float(zp, zn)
            data.setClean(dcReader.zValues)

            sOutput = data.outputValue(dcReader.message)
            sOutput.setString('dcReader. v1.0. David Cuellar. www.davidcuellar.es')
            data.setClean(dcReader.message)

    @staticmethod
    def return_matrix_vectors(matrix,
                              vx=om.MFloatVector.kXaxisVector,
                              vy=om.MFloatVector.kYaxisVector,
                              vz=om.MFloatVector.kZaxisVector):
        '''
        :param matrix: world matrix
        :param vx: vector x
        :param vy: vector y
        :param vz: vector z
        :return: vy, vy, vz, tv (translation vector)
        '''
        vx = (vx * matrix).normal()
        vy = (vy * matrix).normal()
        vz = (vz * matrix).normal()
        tv = om.MFloatVector(matrix.getElement(3, 0), matrix.getElement(3, 1), matrix.getElement(3, 2))
        return vx, vy, vz, tv

    @staticmethod
    def get_aim_vector(posA, posB):
        '''
        :param posA: Point A. Reader position
        :param posB: Point B. Target position
        :return: aim vector
        '''
        v = om.MFloatVector([posA[0] - posB[0], posA[1] - posB[1], posA[2] - posB[2]]).normal()
        return v

    def rotation_values_normalize(self, ax, ay, az):
        '''
        :param aX: angle X
        :param aY: angle Y
        :param aZ: angle Z
        :return: values from [0.0, 1.0] for each positive and negative axis
        '''
        xPos, xNeg = self.get_unify_values(ax)
        yPos, yNeg = self.get_unify_values(ay)
        zPos, zNeg = self.get_unify_values(az)
        return xPos, xNeg, yPos, yNeg, zPos, zNeg

    def get_angles(self, rX, rY, rZ, aimV):
        '''
        :param rX: reader X vector
        :param rY: reader Y vector
        :param rZ: reader Z vector
        :param aimV: aim vector from reader to target
        :return: angles for each axis in degrees
        '''
        aX = self.radians_to_degrees(rX.angle(aimV))
        aY = self.radians_to_degrees(rY.angle(aimV))
        aZ = self.radians_to_degrees(rZ.angle(aimV))
        return aX, aY, aZ

    @staticmethod
    def get_unify_values(angle):
        if angle > 90:
            pos = abs(round(1 - angle / 90, 3))
            neg = 0.0
        else:
            pos = 0.0
            neg = abs(round(1 - angle / 90, 3))
        return pos, neg

    @staticmethod
    def radians_to_degrees(radians):
        '''
        :param radians: angle in radians
        :return: angle in degrees
        '''
        return abs(round((radians * 180) / math.pi, 3))

    def get_ramp_values(self, xp, xn, yp, yn, zp, zn):
        '''
        :param xp: X positive
        :param xn: X negative
        :param yp: Y positive
        :param yn: Y negative
        :param zp: Z positive
        :param zn: Z negative
        :return: the value for each ramp in that angle position
        '''
        thisNode = self.thisMObject()
        xPosRamp = om.MRampAttribute(thisNode, dcReader.xPosRamp)
        xNegRamp = om.MRampAttribute(thisNode, dcReader.xNegRamp)
        yPosRamp = om.MRampAttribute(thisNode, dcReader.yPosRamp)
        yNegRamp = om.MRampAttribute(thisNode, dcReader.yNegRamp)
        zPosRamp = om.MRampAttribute(thisNode, dcReader.zPosRamp)
        zNegRamp = om.MRampAttribute(thisNode, dcReader.zNegRamp)
        return round(xPosRamp.getValueAtPosition(xp), 3), round(xNegRamp.getValueAtPosition(xn), 3), \
               round(yPosRamp.getValueAtPosition(yp), 3), round(yNegRamp.getValueAtPosition(yn), 3), \
               round(zPosRamp.getValueAtPosition(zp), 3), round(zNegRamp.getValueAtPosition(zn), 3)


def initializePlugin(obj):
    plugin = om.MFnPlugin(obj, "David Cuellar", "1.0", "Any")

    try:
        plugin.registerNode("dcReader", dcReader.id, dcReader.creator, dcReader.initialize, om.MPxNode.kDependNode)
    except:
        sys.stderr.write("Failed to register node\n")
        raise


def uninitializePlugin(obj):
    plugin = om.MFnPlugin(obj)

    try:
        plugin.deregisterNode(dcReader.id)
    except:
        sys.stderr.write("Failed to deregister node\n")
        pass
